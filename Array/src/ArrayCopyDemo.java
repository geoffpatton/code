/**
 * Created by tua26762 on 6/6/2015.
 */
class ArrayCopyDemo {
    public static void main(String[] args) {
        char[] copyFrom = { 'd', 'e', 'c', 'a', 'f', 'f', 'e',
                'i', 'n', 'a', 't', 'e', 'd' };
        //char[] copyTo = new char[7];

        //System.arraycopy(copyFrom, 6, copyTo, 0, 7);

        char[] copyTo = java.util.Arrays.copyOfRange(copyFrom, 2, 9);

        System.out.println(new String(copyTo));
    }
}